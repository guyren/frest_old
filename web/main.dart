import 'dart:html';

import 'package:frest/get_template_for.dart';
import 'package:frest/render_template.dart';
import 'package:frest/store.dart';
import 'package:frest/update_ui_with_html.dart';


void main() {
  var _store = {'/': {'message': 'hello'}};

  var documentUpdater   = updateUIWithHTML({#document: document, #as: '/'});
  var templateRenderer  = renderTemplate({#to: documentUpdater, #as: #it});
  var templateGetter    = getTemplateFor({
                            #from: '/', 
                            #to: templateRenderer, 
                            #as: #template});
  var storeGetter       = storeGet({
                            #store: _store, 
                            #from:  '/', 
                            #to:    templateRenderer,
                            #as:    #it});
}
