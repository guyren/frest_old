import 'package:frest/endpoint/endpoint.dart';
import 'package:mustache_template/mustache.dart';

Endpoint renderTemplate(val) => Endpoint(
  val,
  name: 'renderTemplate',
  required: const {#it, #template, #to, #as},
  fn: (val) {
    var template    = Template(val[#template]);
    val[#to][val[#as]]   = template.renderString(val[#it]);
  }
);
