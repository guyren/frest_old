class Endpoint {
  var val;
  var fn;
  String? name;
  Set<Symbol> required;

  Endpoint(
    $, {
    this.fn,
    this.required = const {},
    this.name
  }) :  val = $ ?? {} {
    call();
  }

  void call([$]) {
    if ($ != null) {
      val += $;
    }

    if (ground) {
      fn(val);
    }
  }

  void operator []=(name, it) {
    val[name] ??= it;

    call();
  }

  Endpoint clone() => Endpoint(val, fn: fn, required: required);

  bool get ground {
    var keys = val?.keys;
    var diff = required.toSet().difference(keys.toSet());
    return keys.toSet().containsAll(required.toSet());
  }
}
