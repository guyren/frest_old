import 'endpoint/endpoint.dart';

Endpoint storeGet(val) => Endpoint(
    val,
    name: 'storeGet',
    required: const {#store, #from, #to, #as},
    fn: (val) => val[#to][val[#as]] = val[#store][val[#from]]);
