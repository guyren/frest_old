import 'endpoint/endpoint.dart';

Endpoint getTemplateFor(val) => Endpoint(
  val,
  name: 'getTamplateFor',
  required: const {#from, #to, #as},
  fn: (val) => val[#to][#template] = '<h1>{{message}}');
