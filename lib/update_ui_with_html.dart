import 'endpoint/endpoint.dart';

Endpoint updateUIWithHTML(val) => Endpoint(
    val,
    name: 'updateUIWithHTML',
    required: const {#document, #it, #as},
    fn:       (val) {
      val[#document]?.getElementById(val[#as])?.innerHtml = val[#it];
    }
);
